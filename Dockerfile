ARG BUILD_ARCH
FROM ${BUILD_ARCH}/redis:5.0.4-alpine

COPY redis.conf /usr/local/etc/redis/redis.conf
COPY run.sh /

CMD [ "/run.sh" ]
