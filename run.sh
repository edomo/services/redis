#!/bin/ash

REDIS_CONF=/usr/local/etc/redis/redis.conf
REDIS_AOF=/data/appendonly.aof

if [[ -s $REDIS_AOF ]]; then
		echo "yes" | redis-check-aof --fix $REDIS_AOF
fi

redis-server $REDIS_CONF
